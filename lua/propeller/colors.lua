return {

  -- mis colores
  fgCode            = '#d1bfae',
  fg                = '#e8d4c0',
  --fg1               = '#ebdcb4',
  fg1               = '#b9aa9b',
  fg2               = '#f8efd8',
  fg3               = '#deccb2',
  fgDocs            = '#a49689',

  --bg0               = '#0f0f15',
  bg0               = '#191919',
  bg                = '#212121',
  bg1               = "#191919",
  bg2               = '#353331',
  bg3               = '#353331',
  bgDark            = '#2b2a29',
  bg00              = '#393837',

  bgSelect          = '#353331',
  bgPanel           = '#4a4642',
  bgPopup           = '#2b2a29',
  lineNumber        = '#605953',
  splitDark         = '#4a4642',

  bgSearch          = '#353331',
  --bgSearchOther     = '#613214',
  bgSearchOther     = '#4a4642',

  grayComment       = '#776e66',
  gray              = '#8d8277',
  grayDark          = '#4a4642',
  context           = '#353331',
  contextCurrent    = '#605953',
  grayLight         = '#d1bfae',

  blue              = '#556e84',
  blue5             = '#4a6474',
  blue6             = '#70a4aa',
  blue4             = '#2a2a37',
  blueDark          = '#2a4254',
	blueMedium        = '#648ba2',
	blueLight         = '#84c1c8',
	blueDeep          = '#566787',
	blueMutted        = '#a5c5c8',
	blueTitle        = '#6d9fa5',

	green             = "#678051",
	greenLight        = "#91aa7b",
	greenBlue         = "#70a4aa",
	greenBlueLight    = "#84c1c8",

	red               = '#C34043',
	redLight          = '#D16969',
	orange            = "#DB784D",
	pink              = '#ce7b62',
	yellow            = '#D7BA7D',
	yellowOrange      = '#e8b276',
	yellowMutted      = '#e8b88f',
	golden            = '#d19b6f',


	purple            = '#9b5d74',
  magenta           = '#aa7b91',

  cursor_fg         = '#515151',
  cursor_bg         = '#AEAeAe',

  error_red         = '#de5565',
  warning_orange    = '#ffa066',
  info_yellow       = '#d7ba7d',

  hint_blue = '#84c1c8',
  ui_blue2 = '#648ba2',
  ui_blue = '#363636',
  ui_blue0 = '#264F78',
  diff_add = '#367366',
  diff_change = '#0c7d9d',
  diff_delete = '#c34043',
  diff_text = '#264f78',
  diff_unstage = '#ce9178',



  dark = '#252525',
  accent = '#BBBBBB',
  popup_back = '#2D2D30',
  search_blue = '#5e81ac',
  light_gray = '#c8c9c1',
  cyan = '#4EC9B0',
  light_green = '#B5CEA8',

  -- kanagawa colores
  -- Bg Shades
    kw_bg0      = "#16161D",
    kw_bg1     = "#181820",
    kw_bg2      = "#1F1F28",
    kw_bg3      = "#2A2A37",
    kw_bg4      = "#363646",
    kw_bg5      = "#54546D",

    -- Popup and Floats
    waveBlue1     = "#223249",
    waveBlue2     = "#2D4F67",

    -- Diff and Git
    winterGreen   = "#2B3328",
    winterYellow  = "#49443C",
    winterRed     = "#43242B",
    kw_wBlue      = "#252535",
    autumnGreen   = "#76946A",
    autumnRed     = "#C34043",
    autumnYellow  = "#DCA561",

    -- Diag
    samuraiRed    = "#E82424",
    roninYellow   = "#FF9E3B",
    waveAqua1     = "#6A9589",
    kw_dBlue      = "#658594",

    -- Fg and Comments
    oldWhite      = "#C8C093",
    fujiWhite     = "#DCD7BA",
    kw_fGray      = "#727169",
    springViolet1 = "#938AA9",

    oniViolet     = "#957FB8",
    crystalBlue   = "#7E9CD8",
    springViolet2 = "#9CABCA",
    springBlue    = "#7FB4CA",
    lightBlue     = "#A3D4D5", -- unused yet
    waveAqua2     = "#7AA89F", -- improve lightness: desaturated greenish Aqua

    -- waveAqua2  = "#68AD99",
    -- waveAqua4  = "#7AA880",
    -- waveAqua5  = "#6CAF95",
    -- waveAqua3  = "#68AD99",

    springGreen   = "#98BB6C",
    boatYellow1   = "#938056",
    boatYellow2   = "#C0A36E",
    carpYellow    = "#E6C384",

    sakuraPink    = "#D27E99",
    waveRed       = "#E46876",
    peachRed      = "#FF5D62",
    surimiOrange  = "#FFA066",
    katanaGray    = "#717C7C",

}

