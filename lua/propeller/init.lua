local util = require 'propeller.util'
local theme = require 'propeller.theme'

vim.o.background = 'dark'
vim.g.colors_name = 'propeller'

util.load(theme)

